﻿// <copyright file="ScoreVisual.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;

    internal class ScoreVisual : FrameworkElement
    {
        private string[] readText;
        private int szamlalo = 0;

        public ScoreVisual()
        {
            string path = "../../eredmenyek.txt";
            try
            {
                this.readText = File.ReadAllLines(path);
            }
            catch (FileNotFoundException)
            {
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(new SolidColorBrush(Colors.Red), null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            if (this.readText != null)
            {
                for (int i = this.readText.Length-1; i > -1; i--)
                {
                    drawingContext.DrawText(new FormattedText(this.readText[i], CultureInfo.GetCultureInfo("hu"), FlowDirection.LeftToRight, new Typeface("Verdana"), 15, Brushes.Black), new Point(0, this.szamlalo));
                    this.szamlalo += 20;
                }
            }
        }
    }
}
