﻿// <copyright file="ISpriteImage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System.Windows.Media;

    internal interface ISpriteImage
    {
        ImageSource GetSprite { get; set; }
    }
}
