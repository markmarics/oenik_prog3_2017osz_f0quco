﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    public abstract class GameObject
    {
        private Point position;
        private static Random rnd = new Random();
        private Geometry geometry;
        private double szélesség;
        private double magasság;
        private double gyorsulas;
        private Size teglalapméret;

        protected GameObject()
        {
        }

        protected GameObject(MapVisual game)
        {
            this.Game = game;
        }

        public delegate void GameObjectEventHandler(GameObject obj);

        public static Random Rnd
        {
            get
            {
                return rnd;
            }

            set
            {
                rnd = value;
            }
        }

        public double Magasság
        {
            get
            {
                return this.magasság;
            }

            set
            {
                this.magasság = value;
            }
        }

        public double Szélesség
        {
            get
            {
                return this.szélesség;
            }

            set
            {
                this.szélesség = value;
            }
        }

        public Point Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        public Geometry GetGeometry
        {
            get
            {
                return this.geometry;
            }

            set
            {
                this.geometry = value;
            }
        }

        public Size Teglalapméret
        {
            get
            {
                return this.teglalapméret;
            }

            set
            {
                this.teglalapméret = value;
            }
        }

        public double Gyorsulas
        {
            get
            {
                return gyorsulas;
            }

            set
            {
                gyorsulas = value;
            }
        }

        protected MapVisual Game { get; private set; }

        public bool Collide(GameObject other)
        {
            PathGeometry intersection = Geometry.Combine(
                this.GetGeometry,
                other.GetGeometry,
                GeometryCombineMode.Intersect,
                null);
            return intersection.GetArea() > 0;
        }

        protected static double Lerp(double a, double b, double t)
        {
            return a + ((b - a) * t);
        }
    }
}
