﻿// <copyright file="Bullet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System.Windows;
    using System.Windows.Media;

    public class Bullet : DynamicObject
    {
        public Bullet(MapVisual game, Karakter karakter)
            : base(game)
        {
            this.Magasság = 4;
            this.Szélesség = 4;
            this.Speed = 30;
            this.Gyorsulas = 0.6;
            this.Position = new Point(karakter.Position.X, karakter.Position.Y + (karakter.Magasság / 2));
            this.GetGeometry = new EllipseGeometry(this.Position, this.Magasság, this.Szélesség);
        }

        public void MoveLeft()
        {
            this.Dx = this.Position.X - this.Speed;
            this.Position = new Point(Lerp(this.Position.X, this.Dx, this.Gyorsulas), this.Position.Y);
            this.GetGeometry = new EllipseGeometry(this.Position, this.Magasság, this.Szélesség);
            this.CheckLeaveScreen();
        }

        public void MoveRight()
        {
            this.Dx = this.Position.X + this.Speed;
            this.Position = new Point(Lerp(this.Position.X, this.Dx, this.Gyorsulas), this.Position.Y);
            this.GetGeometry = new EllipseGeometry(this.Position, this.Magasság, this.Szélesség);
            this.CheckLeaveScreen();
        }
    }
}
