﻿// <copyright file="Karakter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Beadando
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;

    public class Karakter : DynamicObject, ISpriteImage
    {
        private ImageSource sprite = null;
        private ObservableCollection<Eletjelzo> elet = new ObservableCollection<Eletjelzo>();
        private Kard kard;
        private bool irany;
        private Dictionary<Bullet, bool> bullets = new Dictionary<Bullet, bool>();
        private int pont;
        private string nev;
        private bool win;

        public Karakter(MapVisual game)
            : base(game)
        {
            this.Magasság = 50;
            this.Szélesség = 20;
            this.Teglalapméret = new Size(this.Szélesség, this.Magasság);
            this.Position = new Point(Rnd.Next(0, 500), Rnd.Next(0, 500));
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            this.Speed = 25;
            this.Ugras = 150;
            this.Gyorsulas = 0.3;
        }

        public ImageSource GetSprite
        {
            get
            {
                return this.sprite;
            }

            set
            {
                this.sprite = value;
            }
        }

        public ObservableCollection<Eletjelzo> Elet
        {
            get
            {
                return this.elet;
            }
        }

        public Kard Kard
        {
            get
            {
                return this.kard;
            }

            set
            {
                this.kard = value;
            }
        }

        public Dictionary<Bullet, bool> Bullets
        {
            get
            {
                return this.bullets;
            }

            set
            {
                this.bullets = value;
            }
        }

        public int Pont
        {
            get
            {
                return this.pont;
            }

            set
            {
                this.pont = value;
            }
        }

        public string Nev
        {
            get
            {
                return this.nev;
            }

            set
            {
                this.nev = value;
            }
        }

        public bool Win
        {
            get
            {
                return this.win;
            }

            set
            {
                this.win = value;
            }
        }

        public void Zuhan()
        {
            this.Gravity = 8.5f;
            this.Dy = this.Position.Y + this.Gravity;
            this.Position = new Point(this.Position.X, this.Dy);
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));

            if (this.Position.Y - this.Magasság > this.Game.ActualHeight)
            {
                this.Position = new Point(this.Position.X, 0);
                this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            }
        }

        public void MoveLeft()
        {
            this.Dx = this.Position.X - this.Speed;
            this.Position = new Point(Lerp(this.Position.X, this.Dx, this.Gyorsulas), this.Position.Y);
            if (this.Position.X < 0)
            {
                this.Position = new Point(this.Game.ActualWidth, this.Position.Y);
                this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            }
            else
            {
                this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            }
        }

        public void MoveRight()
        {
            this.Dx = this.Position.X + this.Speed;
            this.Position = new Point(Lerp(this.Position.X, this.Dx, this.Gyorsulas), this.Position.Y);

            if (this.Position.X + this.Szélesség > this.Game.ActualWidth)
            {
                this.Position = new Point(0, this.Position.Y);
                this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            }
            else
            {
                this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            }
        }

        public void Jump()
        {
            this.Dy = this.Position.Y - this.Ugras;
            this.Gravity = 0;
            this.Position = new Point(this.Position.X, Lerp(this.Position.Y, this.Dy, this.Gyorsulas));
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
        }

        public void Életetveszít()
        {
            for (int i = 0; i < this.elet.ToArray().Length; i++)
            {
                if (this.elet.ToArray()[i] == this.Életetkeres())
                {
                    this.elet.RemoveAt(i);
                }
            }

        }

        public void Vág()
        {
            this.kard = new Kard(this);
        }

        public void Lövés()
        {
            Bullet b = new Bullet(this.Game, this);
            this.bullets.Add(b, this.irany);
            b.LeaveScreen += this.B_LeaveScreen;
        }

        public void Lovesirany(bool irany)
        {
            this.irany = irany;
        }

        protected Eletjelzo Életetkeres()
        {
            return this.elet.ElementAt(this.elet.IndexOf(this.elet.Last()));
        }

        private void B_LeaveScreen(GameObject obj)
        {
            this.bullets.Remove(obj as Bullet);
        }
    }
}
