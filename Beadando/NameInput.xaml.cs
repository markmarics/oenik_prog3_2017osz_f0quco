﻿// <copyright file="NameInput.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for NameInput.xaml
    /// </summary>
    public partial class NameInput : Window
    {
        public NameInput()
        {
            this.KeyDown += this.Enter;
            this.InitializeComponent();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Enter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Close();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.Nev1.Text == string.Empty && this.Nev2.Text == string.Empty)
            {
              if (MessageBox.Show("Kötelező nevet megadni! Ki akarsz lépni?", "Név megadása", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    MainWindow mw = new MainWindow();
                    mw.Show();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                GameWindow gw = new GameWindow();
                gw.mapvisual.Karakterek[0].Nev = this.Nev1.Text;
                gw.mapvisual.Karakterek[1].Nev = this.Nev2.Text;
                gw.Show();
            }
        }

        private new void PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = e.Key == Key.Space;
        }
    }
}
