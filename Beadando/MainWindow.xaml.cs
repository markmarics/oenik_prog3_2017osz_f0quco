﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SajatButton sb0;
        private SajatButton sb1;
        private SajatButton sb2;
        private SajatButton sb3;

        public MainWindow()
        {
            this.InitializeComponent();
            this.background.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/Resources/hatter.jpg"));
            StackPanel sp = new StackPanel()
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(50, 60, 0, 0)
            };
            this.sb0 = new SajatButton() { Text = "New game" };
            this.sb1 = new SajatButton() { Text = "Scores" };
            this.sb2 = new SajatButton() { Text = "Controls" };
            this.sb3 = new SajatButton() { Text = "Exit" };

            sp.Children.Add(this.sb0);
            sp.Children.Add(this.sb1);
            sp.Children.Add(this.sb2);
            sp.Children.Add(this.sb3);
            this.grid.Children.Add(sp);
            this.sb0.Click += this.NewGame;
            this.sb1.Click += this.Scores;
            this.sb2.Click += this.Controls;
            this.sb3.Click += this.QuitGame;
        }

        private void Controls(object sender, MouseButtonEventArgs e)
        {
            IranyitasWindow iw = new IranyitasWindow();
            iw.Show();
            this.Close();
        }

        private void Scores(object sender, MouseButtonEventArgs e)
        {
            ScoreWindow sw = new ScoreWindow();
            sw.Show();
            this.Close();
        }

        private void QuitGame(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void NewGame(object sender, MouseButtonEventArgs e)
        {
            NameInput ni = new NameInput();
            ni.Show();
            this.Close();
        }
    }
}
