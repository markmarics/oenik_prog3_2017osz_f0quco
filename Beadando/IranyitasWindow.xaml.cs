﻿// <copyright file="IranyitasWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for IranyitasWindow.xaml
    /// </summary>
    public partial class IranyitasWindow : Window
    {
        public IranyitasWindow()
        {
            this.InitializeComponent();
            this.Closing += this.IranyitasWindow_Closing;
        }

        private void IranyitasWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Ki akarsz lépni?", "Irányítás", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                MainWindow mw = new MainWindow();
                mw.Show();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
