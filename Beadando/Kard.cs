﻿// <copyright file="Kard.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public class Kard : DynamicObject, ISpriteImage
    {
        private static ImageSource sprite = null;
        private Karakter karakter;

        public Kard(Karakter karakter)
        {
            this.karakter = karakter;
            this.Position = karakter.Position;
            this.Szélesség = 50;
            this.Magasság = 20;
            this.Teglalapméret = new Size(this.Szélesség, this.Magasság);
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position.X + (karakter.Szélesség / 2), karakter.Position.Y + (karakter.Magasság / 2), this.Teglalapméret.Width, this.Teglalapméret.Height));
            sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/katanaright.png"));
        }

        public ImageSource GetSprite
        {
            get
            {
                return sprite;

            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public void Forgatbalra()
        {
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position.X - this.Szélesség, this.karakter.Position.Y + (this.karakter.Magasság / 2), this.Teglalapméret.Width, this.Teglalapméret.Height));
            sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/katanaleft.png"));
        }
    }
}
