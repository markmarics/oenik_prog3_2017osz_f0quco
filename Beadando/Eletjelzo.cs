﻿/// <summary>
/// Beadando: namespace
/// </summary>
/// <returns>Beadando</returns>
namespace Beadando
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public class Eletjelzo : GameObject, ISpriteImage
    {
        private static ImageSource sprite = null;

        public Eletjelzo(MapVisual game, Point hely)
            : base(game)
        {
            this.Szélesség = 50;
            this.Magasság = 30;
            this.Position = hely;
            this.Teglalapméret = new Size(this.Szélesség, this.Magasság);
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            if (sprite == null)
            {
                sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/katana.png"));
            }
        }

        public ImageSource GetSprite
        {
            get
            {
                return sprite;
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
