﻿// <copyright file="Teglalap.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public class Teglalap : GameObject, ISpriteImage
    {
        private static ImageSource sprite = null;

        public Teglalap(MapVisual game)
           : base(game)
        {
            this.Magasság = 20;
            this.Szélesség = 80;
            this.Teglalapméret = new Size(this.Szélesség, this.Magasság);
            this.Position = new Point(Rnd.Next(0 + 20, Convert.ToInt32(game.ActualWidth - this.Szélesség - 20)), Rnd.Next(0 + 50, Convert.ToInt32(game.ActualHeight - this.Magasság - 50)));
            this.GetGeometry = new RectangleGeometry(new Rect(this.Position, this.Teglalapméret));
            if (sprite == null)
            {
                sprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/wall.png"));
            }
        }

        public ImageSource GetSprite
        {
            get
            {
                return sprite;
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
