﻿// <copyright file="MapVisual.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    public class MapVisual : FrameworkElement
    {
        private bool palyakesz = false;
        private bool jatekvege = false;
        private bool left;
        private bool right;
        private bool ugras;
        private bool a;
        private bool d;
        private bool w;
        private bool down = true;
        private bool s = true;
        private bool vágR;
        private bool vágL;
        private bool loves1;
        private bool loves2;
        private int tolteny1;
        private int tolteny2;
        private DispatcherTimer dt;
        private BitmapImage bitmaphatter;
        private ObservableCollection<Teglalap> palyaelemek;
        private ObservableCollection<Karakter> karakterek;

        public MapVisual()
        {
            this.bitmaphatter = new BitmapImage(new Uri(@"pack://application:,,,/Resources/" + Háttér));
            this.palyaelemek = new ObservableCollection<Teglalap>();
            this.Karakterek = new ObservableCollection<Karakter>();
            this.Karakterek.Add(new Karakter(this));
            this.Karakterek.Add(new Karakter(this));
            this.Karakterek[0].GetSprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/karakter.png"));
            this.Karakterek[1].GetSprite = new BitmapImage(new Uri(@"pack://application:,,,/Resources/karakter2.png"));
            this.Loaded += this.OnLoaded;
        }

        public static string Háttér
        {
            get
            {
                Random r = new Random();
                int hatterszam = r.Next(0, 3);
                switch (hatterszam)
                {
                    case 0:
                        return "background.png";
                    case 1:
                        return "background2.png";
                    case 2:
                        return "background3.png";
                    default:
                        return null;
                }
            }
        }

        public bool Palyakesz
        {
            get
            {
                return this.palyakesz;
            }

            set
            {
                this.palyakesz = value;
            }
        }

        public bool Jatekvege
        {
            get
            {
                return this.jatekvege;
            }

            set
            {
                this.jatekvege = value;
            }
        }

        // int ugrasszamlalo;
        internal ObservableCollection<Karakter> Karakterek
        {
            get
            {
                return this.karakterek;
            }

            set
            {
                this.karakterek = value;
            }
        }

        public void Jatekfajlbament(string nev, string pont, string ellenfél, string ellenfélpont)
        {
            string path = "../../eredmenyek.txt";
            string createtext = nev + " " + pont + " - " + ellenfél + " " + ellenfélpont + Environment.NewLine;
            if (!File.Exists(path))
            {
                File.WriteAllText(path, createtext);
            }
            else
            {
                StreamWriter sw = File.AppendText(path);
                sw.WriteLine(nev + " " + pont + " - " + ellenfél + " " + ellenfélpont);
                sw.Close();
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawImage(this.bitmaphatter, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            if (this.palyaelemek.Count > 0)
            {
                foreach (Teglalap elem in this.palyaelemek)
                {
                    drawingContext.DrawImage(elem.GetSprite, elem.GetGeometry.Bounds);
                }
            }

            foreach (Karakter item in this.Karakterek)
            {
                drawingContext.DrawImage(item.GetSprite, item.GetGeometry.Bounds);
            }

            if (this.Karakterek[0].Bullets.Count > 0)
            {
                foreach (Bullet bullet in this.Karakterek[0].Bullets.Keys)
                {
                    drawingContext.DrawGeometry(Brushes.LightBlue, new Pen(Brushes.DarkBlue, 1), bullet.GetGeometry);
                }
            }

            if (this.Karakterek[1].Bullets.Count > 0)
            {
                foreach (Bullet bullet in this.Karakterek[1].Bullets.Keys)
                {
                    drawingContext.DrawGeometry(Brushes.White, new Pen(Brushes.Black, 1), bullet.GetGeometry);
                }
            }

            if (this.Karakterek[0].Kard != null)
            {
                drawingContext.DrawImage(this.Karakterek[0].Kard.GetSprite, this.Karakterek[0].Kard.GetGeometry.Bounds);
            }

            if (this.Karakterek[1].Kard != null)
            {
                drawingContext.DrawImage(this.Karakterek[1].Kard.GetSprite, this.Karakterek[1].Kard.GetGeometry.Bounds);
            }

            if (this.Karakterek[0].Elet.Count > 0)
            {
                foreach (Eletjelzo item in this.Karakterek[0].Elet.ToArray())
                {
                    drawingContext.DrawImage(item.GetSprite, item.GetGeometry.Bounds);
                }
            }

            if (this.Karakterek[1].Elet.Count > 0)
            {
                foreach (Eletjelzo item in this.Karakterek[1].Elet.ToArray())
                {
                    drawingContext.DrawImage(item.GetSprite, item.GetGeometry.Bounds);
                }
            }

            if (this.karakterek[0].Win && this.karakterek[0].Nev != null)
            {
                drawingContext.DrawImage(new BitmapImage(new Uri(@"pack://application:,,,/Resources/player0win.png")), new Rect(new Point(this.ActualWidth / 4, 0), new Size(this.ActualWidth / 2, this.ActualHeight)));
                drawingContext.DrawText(new FormattedText(this.karakterek[0].Nev + " győzött!", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 100, Brushes.White), new Point(10, 10));
            }

            if (this.karakterek[1].Win && this.karakterek[1].Nev != null)
            {
                drawingContext.DrawImage(new BitmapImage(new Uri(@"pack://application:,,,/Resources/player1win.png")), new Rect(new Point(this.ActualWidth / 4, 0), new Size(this.ActualWidth / 2, this.ActualHeight)));
                drawingContext.DrawText(new FormattedText(this.karakterek[1].Nev + " győzött!", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 100, Brushes.White), new Point(10, 10));
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.tolteny1 = 0;
            this.tolteny2 = 0;
            double eletjelzo1X = 0;
            double eletjelzo2X = this.ActualWidth - 50;
            for (int i = 0; i < 10; i++)
            {
                this.Karakterek[0].Elet.Add(new Eletjelzo(this, new Point(eletjelzo1X, 30)));
                eletjelzo1X += 40;
                this.Karakterek[1].Elet.Add(new Eletjelzo(this, new Point(eletjelzo2X, 30)));
                eletjelzo2X -= 40;
            }

            this.palyaelemek.Add(new Teglalap(this));
            while (!this.Palyakesz)
            {
                int kollájd = 0;
                Teglalap uj = new Teglalap(this);
                foreach (Teglalap item in this.palyaelemek.ToArray())
                {
                    if (uj.Collide(item))
                    {
                        kollájd = 2;
                        break;
                    }
                }

                if (kollájd != 2)
                {
                    if (this.palyaelemek.Count == 30)
                    {
                        this.Palyakesz = true;
                    }
                    else
                    {
                        this.palyaelemek.Add(uj);
                    }
                }
            }

            Window.GetWindow(this).KeyDown += this.MapVisual_KeyDown;
            Window.GetWindow(this).KeyUp += this.MapVisual_KeyUp;
            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 30);
            this.dt.Tick += this.Update;
            this.dt.Start();
            this.InvalidateVisual();
        }

        private void Update(object sender, EventArgs e)
        {
            if (this.down)
            {
                this.Karakterek[0].Zuhan();
            }

            if (this.left)
            {
                this.Karakterek[0].MoveLeft();
            }

            if (this.right)
            {
                this.Karakterek[0].MoveRight();
            }

            if (!this.down)
            {
                if (this.ugras)
                {
                    this.Karakterek[0].Jump();
                    this.down = true;
                    this.ugras = false;
                }
            }

            // 2. karakter mozgás
            if (this.s)
            {
                this.Karakterek[1].Zuhan();
            }

            if (this.a)
            {
                this.Karakterek[1].MoveLeft();
            }

            if (this.d)
            {
                this.Karakterek[1].MoveRight();
            }

            if (!this.s)
            {
                if (this.w)
                {
                    this.Karakterek[1].Jump();
                    this.s = true;
                    this.w = false;
                }
            }

            // lövés
            if (this.loves1 && this.tolteny1 < 10)
            {
                if (this.left && !this.right)
                {
                    this.Karakterek[0].Lovesirany(true);
                    this.Karakterek[0].Lövés();
                    this.loves1 = false;
                    this.tolteny1++;
                }
                else if (this.right && !this.left)
                {
                    this.Karakterek[0].Lovesirany(false);
                    this.Karakterek[0].Lövés();
                    this.loves1 = false;
                    this.tolteny1++;
                }
            }

            // 2. karakter lövés
            if (this.loves2 && this.tolteny2 < 10)
            {
                if (this.a && !this.d)
                {
                    this.Karakterek[1].Lovesirany(true);
                    this.Karakterek[1].Lövés();
                    this.loves1 = false;
                    this.tolteny2++;
                }
                else if (this.d && !this.a)
                {
                    this.Karakterek[1].Lovesirany(false);
                    this.Karakterek[1].Lövés();
                    this.loves1 = false;
                    this.tolteny2++;
                }
            }

            foreach (Teglalap elem in this.palyaelemek)
            {
                if (elem.Collide(this.Karakterek[0]) && elem.Position.Y > (this.Karakterek[0].Position.Y + this.Karakterek[0].Magasság - 10))
                {
                    this.down = false;
                    break;
                }
                else
                {
                    this.down = true;
                }
            }

            // 2. karakter ütközése a téglalapokkal
            foreach (Teglalap elem2 in this.palyaelemek)
            {
                if (elem2.Collide(this.Karakterek[1]) && elem2.Position.Y > (this.Karakterek[1].Position.Y + this.Karakterek[1].Magasság - 10))
                {
                    this.s = false;
                    break;
                }
                else
                {
                    this.s = true;
                }
            }

            if (this.vágL)
            {
                if (this.left)
                {
                    this.Karakterek[0].Vág();

                    this.Karakterek[0].Kard.Forgatbalra();
                }
                else
                {
                    this.Karakterek[0].Vág();
                }

                if (this.Karakterek[0].Kard.Collide(this.Karakterek[1]))
                {
                    this.Karakterek[0].Pont += 100;
                    this.Karakterek[1].Életetveszít();
                }
            }

            // 2. karakter vágás
            if (this.vágR)
            {
                if (this.a)
                {
                    this.Karakterek[1].Vág();
                    this.Karakterek[1].Kard.Forgatbalra();
                }
                else
                {
                    this.Karakterek[1].Vág();
                }

                if (this.Karakterek[1].Kard.Collide(this.Karakterek[0]))
                {
                    this.Karakterek[1].Pont += 100;
                    this.Karakterek[0].Életetveszít();
                }
            }

            // töltények mozgása
            foreach (Bullet item in this.Karakterek[0].Bullets.Keys.ToArray())
            {
                if (this.Karakterek[0].Bullets[item] == true)
                {
                    item.MoveLeft();
                }
                else
                {
                    item.MoveRight();
                }

                if (item.Collide(this.Karakterek[1]))
                {
                    this.karakterek[0].Pont += 500;
                    this.Karakterek[1].Életetveszít();
                    this.Karakterek[0].Bullets.Remove(item);
                }

                foreach (Teglalap elem in this.palyaelemek)
                {
                    if (item.Collide(elem))
                    {
                        this.Karakterek[0].Bullets.Remove(item);
                    }
                }
            }

            // töltények mozgása 2. karakter
            foreach (Bullet item in this.Karakterek[1].Bullets.Keys.ToArray())
            {
                if (this.Karakterek[1].Bullets[item] == true)
                {
                    item.MoveLeft();
                }
                else
                {
                    item.MoveRight();
                }

                if (item.Collide(this.Karakterek[0]))
                {
                    this.karakterek[1].Pont += 500;
                    this.Karakterek[0].Életetveszít();
                    this.Karakterek[1].Bullets.Remove(item);
                }

                foreach (Teglalap elem in this.palyaelemek)
                {
                    if (item.Collide(elem))
                    {
                        this.Karakterek[1].Bullets.Remove(item);
                    }
                }
            }

            if (this.Karakterek[0].Elet.Count == 0)
            {
                this.karakterek[1].Win = true;
                this.Jatekfajlbament(this.karakterek[1].Nev, this.karakterek[1].Pont.ToString(), this.karakterek[0].Nev, this.karakterek[0].Pont.ToString());
                this.dt.Stop();
            }

            if (this.Karakterek[1].Elet.Count == 0)
            {
                this.karakterek[0].Win = true;
                this.Jatekfajlbament(this.karakterek[0].Nev, this.karakterek[0].Pont.ToString(), this.karakterek[1].Nev, this.karakterek[1].Pont.ToString());
                this.dt.Stop();
            }

            this.InvalidateVisual();
        }

        private void MapVisual_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                this.left = false;
            }

            if (e.Key == Key.Right)
            {
                this.right = false;
            }

            if (e.Key == Key.Up)
            {
                this.ugras = false;
            }

            if (e.Key == Key.A)
            {
                this.a = false;
            }

            if (e.Key == Key.D)
            {
                this.d = false;
            }

            if (e.Key == Key.W)
            {
                this.w = false;
            }

            if (e.Key == Key.R)
            {
                this.vágR = false;
                this.Karakterek[1].Kard = null;
            }

            if (e.Key == Key.L)
            {
                this.vágL = false;
                this.Karakterek[0].Kard = null;
            }

            if (e.Key == Key.T)
            {
                this.loves2 = false;
            }

            if (e.Key == Key.K)
            {
                this.loves1 = false;
            }
        }

        private void MapVisual_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                this.left = true;
            }

            if (e.Key == Key.Right)
            {
                this.right = true;
            }

            if (e.Key == Key.Up)
            {
                this.ugras = true;
            }

            if (e.Key == Key.A)
            {
                this.a = true;
            }

            if (e.Key == Key.D)
            {
                this.d = true;
            }

            if (e.Key == Key.W)
            {
                this.w = true;
            }

            if (e.Key == Key.R)
            {
                this.vágR = true;
            }

            if (e.Key == Key.L)
            {
                this.vágL = true;
            }

            if (e.Key == Key.T)
            {
                this.loves2 = true;
            }

            if (e.Key == Key.K)
            {
                this.loves1 = true;
            }
        }
    }
}
