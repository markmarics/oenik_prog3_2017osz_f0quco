﻿// <copyright file="DynamicObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    public abstract class DynamicObject : GameObject
    {
        private const double Globalspeed = 1;
        private double speed;
        private double dx;
        private double dy;
        private double gravity = 3;
        private double ugras;

        protected DynamicObject(MapVisual game)
            : base(game)
        {
        }

        protected DynamicObject()
        {
        }

        public event GameObjectEventHandler LeaveScreen;

        protected double Speed
        {
            get
            {
                return this.speed;
            }

            set
            {
                this.speed = value;
            }
        }

        protected double Dx
        {
            get
            {
                return this.dx;
            }

            set
            {
                this.dx = value;
            }
        }

        protected double Dy
        {
            get
            {
                return this.dy;
            }

            set
            {
                this.dy = value;
            }
        }

        protected double Ugras
        {
            get
            {
                return this.ugras;
            }

            set
            {
                this.ugras = value;
            }
        }

        protected double Gravity
        {
            get
            {
                return this.gravity;
            }

            set
            {
                this.gravity = value;
            }
        }

        public void CheckLeaveScreen()
        {
            if (this.Position.X > this.Game.ActualWidth || this.Position.X < 0)
            {
                this.LeaveScreen?.Invoke(this);
            }
        }
    }
}
