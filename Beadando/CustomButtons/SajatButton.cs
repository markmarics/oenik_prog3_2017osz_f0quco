﻿// <copyright file="SajatButton.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Beadando
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    public sealed class SajatButton : Grid
    {
        // textures:
        private BitmapImage texnormal;

        // textures:
        private BitmapImage texhover;

        // textures:
        private BitmapImage texhoverOnly;

        // textures:
        private BitmapImage texonClick;

        // ui components:
        private Image hoverImage;
        private double hoverAlpha = 0f;
        private float pulseSpeed = 2f;
        private Label labelText;

        // timer:
        private DispatcherTimer dt;

        // flags:
        private bool isMouseHovering = false;
        private bool isMouseBeignPressed = false;

        public SajatButton()
        {
            this.dt = new DispatcherTimer();
            this.dt.Interval = new TimeSpan(0, 0, 0, 0, 30);
            this.dt.Tick += this.Update;
            this.dt.Start();
            this.texnormal = new BitmapImage(new Uri(@"pack://application:,,,/Resources/SajatButton_normal.png"));
            this.texhover = new BitmapImage(new Uri(@"pack://application:,,,/Resources/SajatButton_hover.png"));
            this.texhoverOnly = new BitmapImage(new Uri(@"pack://application:,,,/Resources/SajatButton_hover_only.png"));
            this.texonClick = new BitmapImage(new Uri(@"pack://application:,,,/Resources/SajatButton_clicked.png"));
            this.Background = new ImageBrush(this.texnormal);
            this.Width = 260;
            this.Height = 80;
            this.hoverImage = new Image()
            {
                Source = this.texhoverOnly,
                Opacity = 0f
            };
            base.Children.Add(this.hoverImage);

            this.labelText = new Label()
            {
                FontSize = 20,
                Foreground = Brushes.DarkOrange,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                Padding = new Thickness(50, 15, 10, 16)
            };
            base.Children.Add(this.labelText);
            this.MouseDown += this.OnMouseDown;
            this.MouseUp += this.OnMouseUp;
            this.MouseEnter += this.OnMouseEnter;
            this.MouseLeave += this.OnMouseLeave;
        }

        public event MouseButtonEventHandler Click;

        [Obsolete("Use Text property instead.", true)]
        public new UIElementCollection Children
        {
            get; private set;
        }

        public string Text
        {
            get
            {
                return (string)this.labelText.Content;
            }

            set
            {
                this.labelText.Content = value;
            }
        }

        public float HoverPulseSpeed
        {
            get
            {
                return this.pulseSpeed;
            }

            set
            {
                this.pulseSpeed = Math.Abs(value);
            }
        }

        private double HoverAlpha
        {
            set
            {
                if (this.hoverAlpha != value)
                {
                    this.hoverAlpha = value;
                    this.hoverImage.Opacity = this.hoverAlpha;
                }
            }
        }

        private bool IsMouseBeignPressed
        {
            set
            {
                if (this.isMouseBeignPressed != value)
                {
                    this.isMouseBeignPressed = value;
                    if (value)
                    {
                        this.Background = new ImageBrush(this.texonClick);
                    }
                    else
                    {
                        this.Background = new ImageBrush(this.texnormal);
                    }
                }
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.IsMouseBeignPressed = true;

            if (this.Click != null)
            {
                this.Click(sender, e);
            }
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.IsMouseBeignPressed = false;
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            this.isMouseHovering = true;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            this.isMouseHovering = false;
        }

        private void Update(object sender, EventArgs e)
        {
            if (this.isMouseHovering && !this.isMouseBeignPressed)
            {
                this.HoverAlpha = (Math.Abs(Math.Sin(TimeSpan.FromTicks(DateTime.Now.Ticks).TotalSeconds * this.pulseSpeed)) / 2) + 0.5f;
            }
            else
            {
                this.HoverAlpha = 0f;
            }
        }
    }
}
